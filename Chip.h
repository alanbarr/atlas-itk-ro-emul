// Author: Beojan Stanislaus <beojan.stanislaus@queens.ox.ac.uk>
// This header implements an abstract base class which classes
// implementing individual chips must be derived from.
//
// The TRE framework accesses chip objects through a pointer to Chip
//
#ifndef CHIP_H
#define CHIP_H
#include <vector>

/** \brief Chip abstract base class.
 *
 *  All chip classes inherit from this class, and provide this interface.
 *  They should also implement CHIPNAME::signalPacket and CHIPNAME::replyPacket,
 *  inheriting from Chip::packet, and additionally providing an interface to access
 *  parts of the bitstream by name.
 *
 *  A CHIPNAME::configuration class should also be provided, constructable from a
 *  msgpack::object* (see \sa{constructChip} function), and the CHIPNAME constructor should
 * take a pointer to a CHIPNAME::configuration (which CHIPNAME is then responsible for deleting)
 */
class Chip {
  public:
    class packet {
      public:
        /// \brief Destructor.
        virtual ~packet() {}

        /** \brief XOR with pattern.
         *
         *  \param pattern [in] Bit pattern to XOR packet bitstring with.
         *                      Must have same length as packet.
         */
        virtual void xorWith(const char* pattern) = 0;

        /** \brief Get bitstring as array of const char.
         *
         *  This bitstring cannot be modified.
         *  This pointer must **not** be deleted by the caller.
         */
        virtual const char* getBitstring() = 0;

        ///\brief Get length of packet in bits.
        virtual int bitLength() = 0;

        // provides length in bytes
        // in header to allow inlining.
        ///\brief Get length of packet in bytes.
        const int length();
    };
    ///\brief Destructor.
    virtual ~Chip() {}

    /** \brief Process signal packet.
     *
     *  Process signal packet, and returns reply packet.
     *  CHIPNAME class should provide version taking
     *  CHIPNAME::signalPacket*,and implement this function
     *  using that one (static_casting the pointers either way).
     *
     *  \param signal Signal packet (usually CHIPNAME::signalPacket*)
     *  \param[out] replyPackets Vector will be filled with reply packets
     */
    virtual void process(packet* signal, std::vector<packet*>& replyPackets) = 0;
};

inline const int Chip::packet::length() {
    if (this->bitLength() % 8 == 0)
        return this->bitLength() / 8;
    else
        return (this->bitLength() / 8) + 1;
}
#endif
