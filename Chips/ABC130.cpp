// Implements ABC130 Chip (Specification version 4.6)
// Author: Beojan Stanislaus <beojan.stanislaus@queens.ox.ac.uk>

#ifdef DEBUG
#undef DEBUG
#define DEBUG 1
#else
#define DEBUG 0
#endif
#include "ABC130.h"
#include "../utilities.h"

#include <stdexcept>
#include <vector>
#include <new>
#include <bitset>
#include <cstdint>
#include <chrono>
#include <iostream>

ABC130::signalPacket::signalPacket(const char* payload, int payloadLength) : m_bitstring(nullptr) {
    if (payloadLength != 2)
        throw new std::runtime_error("INVALID SIGNAL LENGTH");
    // convert payload to bitset
    std::bitset<12> bitstr = to_bitset<12>(payload);
    // validate
    if (bitstr[0] != 1 || bitstr[11] != 0 || bitstr[1] == bitstr[2])
        throw new std::runtime_error("INVALID SIGNAL");
    // decode signal type
    if (bitstr[1])
        signalType = L1;
    else
        signalType = R3;
    // extract L0ID
    L0ID = 0;
    for (int i = 0; i < 8; i++) {
        L0ID |= (static_cast<char>(bitstr[i + 3]) << (7 - i));
    }
    // initialize m_toXORWith
    m_toXORWith = new char[2]();
}

ABC130::signalPacket::~signalPacket() {
    delete[] m_toXORWith;
    delete[] m_bitstring;
}

const char* ABC130::signalPacket::getBitstring() {
    if (m_bitstring != nullptr)
        delete[] m_bitstring;
    std::bitset<12> bitstring;
    // encode signal type
    if (signalType == L1) {
        bitstring[0] = 1;
        bitstring[1] = 1;
        bitstring[2] = 0;
    } else {
        bitstring[0] = 1;
        bitstring[1] = 0;
        bitstring[2] = 1;
    }
    // encode L0ID
    for (int i = 0; i < 8; i++) {
        bitstring[i + 3] = L0ID & (1 << (7 - i));
    }
    // stop bit
    bitstring[11] = 0;
    // convert to char array
    m_bitstring = to_charptr<12>(bitstring);

    for (int i = 0; i < length(); i++) {
        m_bitstring[i] ^= m_toXORWith[i];
    }
    return m_bitstring;
}

void ABC130::signalPacket::xorWith(const char* complement) {
    // xor new complement with existing
    for (int i = 0; i < length(); i++) {
        m_toXORWith[i] ^= complement[i];
    }
}

int ABC130::signalPacket::bitLength() { return 12; }

ABC130::replyPacket::replyPacket(ReplyType typ) : TYP(typ), m_bitstring(nullptr) {
    m_toXORWith = new char[8](); // 60 bits is 8 bytes
    // initialize appropriate bitsets using placement new
    // because constructors are not automatically run for
    // fields in a union
    switch (typ) {
    case ReplyType::R3Hits:
    case ReplyType::R3NoHits:
        break;

    case ReplyType::L1_1BCHits:
    case ReplyType::L1_1BCNoHits:
    case ReplyType::L1_1BCLastPacket:
        new (&(L1_1BC.Hit1)) std::bitset<3>(0);
        new (&(L1_1BC.Hit2)) std::bitset<3>(0);
        new (&(L1_1BC.Hit3)) std::bitset<3>(0);
        break;

    case ReplyType::L1_3BCHits:
    case ReplyType::L1_3BCNoHits:
    case ReplyType::L1_3BCLastPacket:
        new (&(L1_3BC.Hit1)) std::bitset<3>(0);
        new (&(L1_3BC.Hit2)) std::bitset<3>(0);
        new (&(L1_3BC.Hit3)) std::bitset<3>(0);
        new (&(L1_3BC.Hit4)) std::bitset<3>(0);
        break;
    default: // non-physics
        break;
    }
}
ABC130::replyPacket::~replyPacket() {
    // destruct bitsets constructed in constructor above
    // Need to call destructors explicitly because they are
    // not automatically called for fields in a union
    switch (TYP) {
    case ReplyType::R3Hits:
    case ReplyType::R3NoHits:
        break;

    case ReplyType::L1_1BCHits:
    case ReplyType::L1_1BCNoHits:
    case ReplyType::L1_1BCLastPacket:
        L1_1BC.Hit1.std::bitset<3>::~bitset<3>();
        L1_1BC.Hit2.std::bitset<3>::~bitset<3>();
        L1_1BC.Hit3.std::bitset<3>::~bitset<3>();
        break;

    case ReplyType::L1_3BCHits:
    case ReplyType::L1_3BCNoHits:
    case ReplyType::L1_3BCLastPacket:
        L1_3BC.Hit1.std::bitset<3>::~bitset<3>();
        L1_3BC.Hit2.std::bitset<3>::~bitset<3>();
        L1_3BC.Hit3.std::bitset<3>::~bitset<3>();
        L1_3BC.Hit4.std::bitset<3>::~bitset<3>();
        break;
    default: // non-physics
        break;
    }
    delete[] m_toXORWith;
    delete[] m_bitstring;
}

const char* ABC130::replyPacket::getBitstring() {
    // generate bitstring in a bitset
    std::bitset<60> bitstring;
    // start bit
    bitstring[0] = 1;
    // ChipID
    for (int i = 0; i < 5; i++) {
        bitstring[i + 1] = (ChipID & (1 << (4 - i))); // i bit of ChipID
    }
    // TYP
    for (int i = 0; i < 4; i++) {
        bitstring[i + 6] = (static_cast<char>(TYP) & (1 << (3 - i)));
    }

    switch (TYP) {
    case ReplyType::R3Hits:
    case ReplyType::R3NoHits:
        // L0ID
        for (int i = 0; i < 8; i++) {
            bitstring[i + 10] = (R3.L0ID & (1 << (7 - i)));
        }
        // BCID
        for (int i = 0; i < 8; i++) {
            bitstring[i + 18] = (R3.BCID & (1 << (7 - i)));
        }
        // Address1
        for (int i = 0; i < 8; i++) {
            bitstring[i + 26] = (R3.Address1 & (1 << (7 - i)));
        }
        // Address2
        for (int i = 0; i < 8; i++) {
            bitstring[i + 34] = (R3.Address2 & (1 << (7 - i)));
        }
        // Address3
        for (int i = 0; i < 8; i++) {
            bitstring[i + 42] = (R3.Address3 & (1 << (7 - i)));
        }
        // Address4
        for (int i = 0; i < 8; i++) {
            bitstring[i + 50] = (R3.Address4 & (1 << (7 - i)));
        }
        // OvF
        bitstring[58] = R3.OvF;
        break;

    case ReplyType::L1_1BCHits:
    case ReplyType::L1_1BCNoHits:
    case ReplyType::L1_1BCLastPacket:
        // L0ID
        for (int i = 0; i < 8; i++) {
            bitstring[i + 10] = (L1_1BC.L0ID & (1 << (7 - i)));
        }
        // BCID
        for (int i = 0; i < 8; i++) {
            bitstring[i + 18] = (L1_1BC.BCID & (1 << (7 - i)));
        }
        // Address1
        for (int i = 0; i < 8; i++) {
            bitstring[i + 26] = (L1_1BC.Address1 & (1 << (7 - i)));
        }
        // Hit1
        for (int i = 0; i < 3; i++) {
            bitstring[i + 34] = L1_1BC.Hit1[i];
        }
        // Address2
        for (int i = 0; i < 8; i++) {
            bitstring[i + 37] = (L1_1BC.Address2 & (1 << (7 - i)));
        }
        // Hit2
        for (int i = 0; i < 3; i++) {
            bitstring[i + 45] = L1_1BC.Hit2[i];
        }
        // Address3
        for (int i = 0; i < 8; i++) {
            bitstring[i + 48] = (L1_1BC.Address3 & (1 << (7 - i)));
        }
        // Hit3
        for (int i = 0; i < 3; i++) {
            bitstring[i + 56] = L1_1BC.Hit3[i];
        }
        break;
    case ReplyType::L1_3BCHits:
    case ReplyType::L1_3BCNoHits:
    case ReplyType::L1_3BCLastPacket:
        // L0ID
        for (int i = 0; i < 8; i++) {
            bitstring[i + 10] = (L1_3BC.L0ID & (1 << (7 - i)));
        }
        // BCID
        for (int i = 0; i < 8; i++) {
            bitstring[i + 18] = (L1_3BC.BCID & (1 << (7 - i)));
        }
        // Address
        for (int i = 0; i < 8; i++) {
            bitstring[i + 26] = (L1_3BC.Address & (1 << (7 - i)));
        }
        // Hit1
        for (int i = 0; i < 3; i++) {
            bitstring[i + 34] = L1_3BC.Hit1[i];
        }
        // Hit2
        for (int i = 0; i < 3; i++) {
            bitstring[i + 37] = L1_3BC.Hit2[i];
        }
        // Hit3
        for (int i = 0; i < 3; i++) {
            bitstring[i + 40] = L1_3BC.Hit3[i];
        }
        // Hit4
        for (int i = 0; i < 3; i++) {
            bitstring[i + 43] = L1_3BC.Hit4[i];
        }
        // TBD -> leave next 13 bits 0
        break;
    default:
        // Address
        for (int i = 0; i < 8; i++) {
            bitstring[i + 10] = (nonPhysics.Address & (1 << (7 - i)));
        }
        // TBD

        // Data
        for (int i = 0; i < 32; i++) {
            bitstring[i + 26] = (nonPhysics.Data & (1 << i));
            // should already be in reversed format
        }
    }
    // stop bit
    bitstring[59] = 1;
    // convert to char array
    char* temp = to_charptr<60>(bitstring);
    if (m_bitstring == nullptr)
        m_bitstring = temp;
    else {
        delete[] m_bitstring;
        m_bitstring = temp;
        // Alternatively, could deep-copy bitstring
        // to avoid invalidating pointers held by caller.
    }

    // error mode
    for (int i = 0; i < length(); i++) {
        m_bitstring[i] ^= m_toXORWith[i];
    }
    return m_bitstring;
}

void ABC130::replyPacket::xorWith(const char* complement) {
    for (int i = 0; i < length(); i++) {
        m_toXORWith[i] ^= complement[i];
    }
}

int ABC130::replyPacket::bitLength() { return 60; }

ABC130::ABC130(const configuration& conf) : config(conf) {
    startTime = std::chrono::high_resolution_clock::now();
}

ABC130::~ABC130() {}

void ABC130::process(signalPacket* signal, std::vector<Chip::packet*>& replyPackets) {
    using namespace std::chrono;
    switch (signal->signalType) {
    case signalPacket::R3: {
#if DEBUG
        std::cout << "Generating R3 Data..." << std::endl;
#endif
        constexpr int numChannels = 256;
        std::bitset<numChannels> channels;
        // generate pattern of hits
        for (int i = 0; i < numChannels; i++) {
            channels[i] = utilities::coinFlip(1.0 / config.recipHitProb);
        }

        replyPacket* reply = channels.any() ? new replyPacket(replyPacket::ReplyType::R3Hits)
                                            : new replyPacket(replyPacket::ReplyType::R3NoHits);
        reply->ChipID = config.ChipID;
        reply->R3.L0ID = signal->L0ID;

        // Gives value of BCCounter now, probably should be changed later
        // 25 represents 25ns (time between bunch crossings)
        reply->R3.BCID =
            ((duration_cast<nanoseconds>(high_resolution_clock::now() - startTime).count() / 25) +
             config.BCCounterInitVal) %
            256;
        reply->R3.OvF = 0;
        int filledAddresses = 0;
        // fill R3 packet
        for (int i = 0; i < channels.size(); i++) {
            if (channels[i]) {
#if DEBUG
                std::cout << "Channel " << i << " hit" << std::endl;
#endif
                if (filledAddresses == 0) {
#if DEBUG
                    std::cout << "Filling address 1" << std::endl;
#endif
                    reply->R3.Address1 = i; // Change once we know how to receive address
                    filledAddresses++;
                } else if (filledAddresses == 1) {
#if DEBUG
                    std::cout << "Filling address 2" << std::endl;
#endif
                    reply->R3.Address2 = i;
                    filledAddresses++;
                } else if (filledAddresses == 2) {
#if DEBUG
                    std::cout << "Filling address 3" << std::endl;
#endif
                    reply->R3.Address3 = i;
                    filledAddresses++;
                } else if (filledAddresses == 3) {
#if DEBUG
                    std::cout << "Filling address 4" << std::endl;
#endif
                    reply->R3.Address4 = i;
                    filledAddresses++;
                } else {
#if DEBUG
                    std::cout << "Overflow" << std::endl;
#endif
                    reply->R3.OvF = 1;
                    filledAddresses++;
                }
            }
        }
        // repeat last address if < 4
        if (filledAddresses == 1) {
            reply->R3.Address4 = reply->R3.Address3 = reply->R3.Address2 = reply->R3.Address1;
        } else if (filledAddresses == 2) {
            reply->R3.Address4 = reply->R3.Address3 = reply->R3.Address2;
        } else if (filledAddresses == 3) {
            reply->R3.Address4 = reply->R3.Address3;
        }
        replyPackets.push_back(reply);
        return;
        break;
    }
    case signalPacket::L1: {
#if DEBUG
        std::cout << "Generating L1 Data..." << std::endl;
#endif
        std::bitset<256> channels;
        // generate pattern of hits
        for (int i = 0; i < 256; i++) {
            channels[i] = utilities::coinFlip(1.0 / config.recipHitProb);

            // After hit, ensure next 3 channels are misses
            if (i > 0 && channels[i - 1])
                channels[i] = 0;
            else if (i > 1 && channels[i - 2])
                channels[i] = 0;
            else if (i > 2 && channels[i - 3])
                channels[i] = 0;
        }
#if DEBUG
        std::cout << "There are " << channels.count() << " clusters." << std::endl;
#endif
        int numPackets = 0;
        if (config.L1_1BC) {
            numPackets =
                (channels.count() % 3 == 0 ? channels.count() / 3 : channels.count() / 3 + 1);
        } else {
            numPackets = channels.count();
        }
        // create and fill L1_1BC or L1_3BC packet as appropriate:
        // No hits
        if (numPackets == 0) {
            if (config.L1_1BC) {
                replyPacket* reply = new replyPacket(replyPacket::ReplyType::L1_1BCNoHits);
                reply->ChipID = config.ChipID;
                reply->L1_1BC.L0ID = signal->L0ID;
                // 25 represents 25ms (time between bunch crossings)
                reply->L1_1BC.BCID = ((duration_cast<nanoseconds>(high_resolution_clock::now() -
                                                                   startTime).count() /
                                       25) +
                                      config.BCCounterInitVal) %
                                     256;
                replyPackets.push_back(reply);
                return;
            } else {
                replyPacket* reply = new replyPacket(replyPacket::ReplyType::L1_3BCNoHits);
                reply->ChipID = config.ChipID;
                reply->L1_3BC.L0ID = signal->L0ID;
                // 25 represents 25ns (time between bunch crossings)
                reply->L1_3BC.BCID = ((duration_cast<nanoseconds>(high_resolution_clock::now() -
                                                                   startTime).count() /
                                       25) +
                                      config.BCCounterInitVal) %
                                     256;
                replyPackets.push_back(reply);
                return;
            }
        }
        // Hits
        if (config.L1_1BC) {
            int currentCluster = 0;
            replyPacket* reply; // pointer var gets reused
            for (int i = 0; i < channels.size(); i++) {
                if (channels[i]) {
#if DEBUG
                    std::cerr << "Channel " << i << " hit (" << (currentCluster + 1)
                              << "th cluster)" << std::endl;
#endif
                    currentCluster++;
                    // create new packet if necessary
                    if (currentCluster % 3 == 1) {
                        if (currentCluster / 3 + 1 == numPackets) { // see calc of numPackets
#if DEBUG
                            std::cerr << "Creating packet " << (currentCluster / 3 + 1)
                                      << "(last packet)" << std::endl;
#endif
                            reply = new replyPacket(replyPacket::ReplyType::L1_1BCLastPacket);
#if DEBUG
                            std::cerr << "Created packet" << std::endl;
#endif
                        } else {
#if DEBUG
                            std::cerr << "Creating packet " << (currentCluster / 3 + 1)
                                      << std::endl;
#endif
                            reply = new replyPacket(replyPacket::ReplyType::L1_1BCHits);
#if DEBUG
                            std::cerr << "Created packet" << std::endl;
#endif
                        }
                        reply->ChipID = config.ChipID;
                        reply->L1_1BC.L0ID = signal->L0ID;

                        // 25 represents 25ns (time between bunch crossings)
                        reply->L1_1BC.BCID =
                            ((duration_cast<nanoseconds>(high_resolution_clock::now() - startTime)
                                  .count() /
                              25) +
                             config.BCCounterInitVal) %
                            256;
                    }
                    // fill packet
                    if (currentCluster % 3 == 1) {
#if DEBUG
                        std::cerr << "Filling address 1" << std::endl;
#endif
                        reply->L1_1BC.Address1 = i;
                        char* hitPattern = utilities::randBitPattern(
                            utilities::poissonCoinFlip(config.meanHitsInCluster, 4) - 1, 3);
                        reply->L1_1BC.Hit1[0] = (*hitPattern) & 0x1; // check 1 bit
                        reply->L1_1BC.Hit1[1] = (*hitPattern) & 0x2; // check 2 bit
                        reply->L1_1BC.Hit1[2] = (*hitPattern) & 0x4; // check 4 bit
                        delete[] hitPattern;
                    }

                    if (currentCluster % 3 == 2) {
#if DEBUG
                        std::cerr << "Filling address 2" << std::endl;
#endif
                        reply->L1_1BC.Address2 = i;
                        char* hitPattern = utilities::randBitPattern(
                            utilities::poissonCoinFlip(config.meanHitsInCluster, 4) - 1, 3);
                        reply->L1_1BC.Hit2[0] = (*hitPattern) & 0x1; // check 1 bit
                        reply->L1_1BC.Hit2[1] = (*hitPattern) & 0x2; // check 2 bit
                        reply->L1_1BC.Hit2[2] = (*hitPattern) & 0x4; // check 4 bit
                        delete[] hitPattern;
                    }

                    if (currentCluster % 3 == 0) { // multiple of 3
#if DEBUG
                        std::cerr << "Filling address 3" << std::endl;
#endif
                        reply->L1_1BC.Address3 = i;
                        char* hitPattern = utilities::randBitPattern(
                            utilities::poissonCoinFlip(config.meanHitsInCluster, 4) - 1, 3);
                        reply->L1_1BC.Hit3[0] = (*hitPattern) & 0x1; // check 1 bit
                        reply->L1_1BC.Hit3[1] = (*hitPattern) & 0x2; // check 2 bit
                        reply->L1_1BC.Hit3[2] = (*hitPattern) & 0x4; // check 4 bit
                        delete[] hitPattern;
                    }
                    // push full packet
                    if (currentCluster % 3 == 0) { // last cluster of packet
                        replyPackets.push_back(reply);
                    }
                }
            }
            // send last packet if number of clusters is not a multiple of 3
            // push last (unfilled) packet if we haven't already
            if (currentCluster % 3 != 0) {
#if DEBUG
                std::cout << "Pushing last packet" << std::endl;
#endif
                // first ensure all addresses are filled
                if (reply->L1_1BC.Address2 = 0) {
                    reply->L1_1BC.Address2 = reply->L1_1BC.Address1;
                    reply->L1_1BC.Hit2 = reply->L1_1BC.Hit1;
                }
                
                if (reply->L1_1BC.Address3 = 0) {
                    reply->L1_1BC.Address3 = reply->L1_1BC.Address2;
                    reply->L1_1BC.Hit3 = reply->L1_1BC.Hit2;
                }
                
                std::cout.flush();
                replyPackets.push_back(reply);
            }
        } else {
            int currentCluster = 0;
            replyPacket* reply; // pointer var gets reused
            for (int i = 0; i < channels.size(); i++) {
                if (channels[i]) {
#if DEBUG
                    std::cout << "Channel " << i << " hit" << std::endl;
#endif
                    currentCluster++;
                    // create packet
                    if (currentCluster == numPackets) {
#if DEBUG
                        std::cout << "Last packet" << std::endl;
#endif
                        reply = new replyPacket(replyPacket::ReplyType::L1_3BCLastPacket);
                    } else {
#if DEBUG
                        std::cout << "Packet with hits" << std::endl;
#endif
                        reply = new replyPacket(replyPacket::ReplyType::L1_3BCHits);
                    }
                    reply->ChipID = config.ChipID;
                    reply->L1_3BC.L0ID = signal->L0ID;

                    // 25 represents 25ns (time between bunch crossings)
                    reply->L1_3BC.BCID = ((duration_cast<nanoseconds>(
                                               high_resolution_clock::now() - startTime).count() /
                                           25) +
                                          config.BCCounterInitVal) %
                                         256;

                    reply->L1_3BC.Address = i;
                    // fill hit patterns
                    // Loop of three bunch crossings
                    // Each time, generate a 4 bit hit pattern, and set accordingly
                    for (int j = 0; j < 3; j++) {
#if DEBUG
                        std::cout << "Generating hit patterns" << std::endl;
#endif
                        char* hitPattern = utilities::randBitPattern(
                            utilities::poissonCoinFlip(config.meanHitsInCluster, 4), 4);
                        reply->L1_3BC.Hit1[j] = *hitPattern & 0x1;
                        reply->L1_3BC.Hit2[j] = *hitPattern & 0x2;
                        reply->L1_3BC.Hit3[j] = *hitPattern & 0x4;
                        reply->L1_3BC.Hit4[j] = *hitPattern & 0x8;
                        delete[] hitPattern;
                    }
                    replyPackets.push_back(reply);
                }
            }
        }
        return;
        break;
    }
    default:
        break;
        // not possible
    }
}

// delegate processing
void ABC130::process(Chip::packet* signal, std::vector<Chip::packet*>& replyPackets) {
    ABC130::signalPacket* sig = static_cast<ABC130::signalPacket*>(signal);
    process(sig, replyPackets);
}
