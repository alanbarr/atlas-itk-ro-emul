// Header for ABC130 Chip
// Author: Beojan Stanislaus <beojan.stanislaus@queens.ox.ac.uk>
// This header contains the implementation of the ABC130 chip
#ifndef ABC130_H
#define ABC130_H

#include <cstdint>
#include <vector>
#include <bitset>
#include <chrono>

#include "../Chip.h"
#include <msgpack.hpp>

/** \brief ABC130 Chip.
 *
 *  This class implements the ABC130 chip.
 */
class ABC130 : public Chip {
  public:
    /** \brief ABC130 Configuration.
     */
    struct configuration {
        /// \brief Five bit chip ID (as binary number)
        unsigned char ChipID;
        /// \brief Reciprocal of probability of each channel being hit
        int recipHitProb;
        /// \brief Mean number of hits in each cluster of 4 channels (Poisson parameter)
        double meanHitsInCluster;
        /// \brief Initial value of L0 counter
        int L0CounterInitVal;
        /// \brief Initial value of BC counter
        int BCCounterInitVal;
        /// \brief Should L1 packets be of 1 bunch crossing format
        bool L1_1BC;
        /// \brief Edge detection
        bool Edge;

        // Implements msgpack pack / unpack
        MSGPACK_DEFINE(ChipID, recipHitProb, meanHitsInCluster, L0CounterInitVal, BCCounterInitVal,
                       L1_1BC, Edge);
    };

    /** \brief ABC130 Signal Packet
     */
    class signalPacket : public Chip::packet {
      public:
        enum SignalType { L1, R3 };
        /// \brief Signal type
        SignalType signalType;
        /// \brief L0ID requested
        unsigned char L0ID;
        explicit signalPacket(const char* payload, int payloadLength);
        ~signalPacket();

        // No copy constructor / assignment
        signalPacket(const signalPacket&) = delete;
        signalPacket& operator=(const signalPacket&) = delete;

        const char* getBitstring();
        void xorWith(const char* complement);
        int bitLength();

      private:
        char* m_toXORWith;
        char* m_bitstring;
    };

    /** \brief ABC130 Reply Packet
     */
    class replyPacket : public Chip::packet {
      public:
        // data members
        /// \brief Chip ID of sender
        unsigned char ChipID;
        enum class ReplyType : unsigned char {
            R3Hits = 0x2,
            R3NoHits = 0x3,
            L1_1BCHits = 0x4,
            L1_1BCLastPacket = 0x6,
            L1_1BCNoHits = 0x7,
            ReadRegister = 0x8,
            StatusRegister = 0x9,
            DCS = 0xA,
            L1_3BCHits = 0xC,
            L1_3BCLastPacket = 0xE,
            L1_3BCNoHits = 0xF
        };

        ///\brief TYP field
        const ReplyType TYP;
        union {
            /// \brief R3 specific fields
            struct {
                unsigned char L0ID;
                unsigned char BCID;
                unsigned char Address1;
                unsigned char Address2;
                unsigned char Address3;
                unsigned char Address4;
                bool OvF;
            } R3;

            /// \brief L1_1BC specific fields
            struct {
                unsigned char L0ID;
                unsigned char BCID;
                unsigned char Address1;
                std::bitset<3> Hit1;
                unsigned char Address2;
                std::bitset<3> Hit2;
                unsigned char Address3;
                std::bitset<3> Hit3;
            } L1_1BC;

            /// \brief L1_3BC specific fields
            struct {
                unsigned char L0ID;
                unsigned char BCID;
                unsigned char Address;
                std::bitset<3> Hit1;
                std::bitset<3> Hit2;
                std::bitset<3> Hit3;
                std::bitset<3> Hit4;
            } L1_3BC;

            /// \brief Fields for non-physics data
            struct {
                unsigned char Address;
                std::uint32_t Data;
            } nonPhysics;
        };

        explicit replyPacket(ReplyType typ);
        ~replyPacket();

        // No copy constructor / assignment
        replyPacket(const replyPacket&) = delete;
        replyPacket& operator=(const replyPacket&) = delete;

        const char* getBitstring();
        void xorWith(const char* complement);
        int bitLength();

      private:
        char* m_toXORWith;
        char* m_bitstring;
    };

    explicit ABC130(const configuration& conf);
    ~ABC130();
    ABC130(const ABC130&) = delete;
    ABC130& operator=(const ABC130&) = delete;

    /** \brief Process signal packet.
     *
     *  Process signal packet, and returns reply packet.
     *
     *  \param signal Signal packet
     *  \param[out] replyPackets Vector will be filled with reply packets
     */

    void process(Chip::packet* signal, std::vector<Chip::packet*>& replyPackets);

    /// \brief Implementation of process() method
    void process(signalPacket* signal, std::vector<Chip::packet*>& replyPackets);

  private:
    configuration config;
    // store creation time of chip for generation of BCID
    std::chrono::high_resolution_clock::time_point startTime;
};

#endif
