// HCC Chip implementation (Specification version 1.5.6+)
// Author: Beojan Stanislaus <beojan.stanislaus@queens.ox.ac.uk>

#include "HCC.h"
#include "../utilities.h"

#include <vector>
//Implement interleave
#include <algorithm>
#include <iterator>
#include <random>
#include <functional>

#include <stdexcept>
#include <iostream>
#include <bitset>
#include <msgpack.hpp>

// convert 5 bit HCC address to a bit position to check in the 14 bit
// addressing string in R3 signals
inline int addressToPos(char n) { return (n % 2) ? (n - 1) / 2 : n / 2; }

// in place reverse the bits in a byte
inline void bitReverseInplace(char& byte) {
    char newByte = 0;
    for (int i = 0; i < 8; i++) {
        if (byte & (1 << i))
            newByte |= (1 << (7 - i));
    }
    byte = newByte;
}

// reverse bits in a byte
inline char bitReverse(char byte) {
    char newByte = 0;
    for (int i = 0; i < 8; i++) {
        if (byte & (1 << i))
            newByte |= (1 << (7 - i));
    }
    return newByte;
}

HCC::signalPacket::signalPacket(const char* payload, int payloadLength) : m_bitstring(nullptr) {
    // validate length
    if (payloadLength != 2 && payloadLength != 4)
        throw new std::runtime_error("INVALID PAYLOAD LENGTH");
    if (payloadLength == 2) { // Should be an L1 request
        // First three bits must be "110" (remember, bit 0 is 1 bit)
        if (!(payload[0] & 0x03) || (payload[1] & 0x10))
            throw new std::runtime_error("INVALID PAYLOAD");
        signalType = L1;
        std::cout << "Constructing L1 packet" << std::endl;
        // Get (bit reversed) L0ID
        char L0ID = (payload[0] >> 3) | (payload[1] << 5);

        this->L0ID = static_cast<unsigned char>(bitReverse(L0ID));
    } else {
        // First three bits must be 101
        if (!(payload[0] & 0x05) || (payload[3] & 0x20))
            throw new std::runtime_error("INVALID PAYLOAD");
        signalType = R3;
        std::cout << "Constructing R3 packet" << std::endl;
        // fill out 14 addressing bits
        for (int i = 3; i < 8; i++) {
            addressing[i - 3] = payload[0] & (1 << i);
        }
        for (int i = 0; i < 8; i++) {
            addressing[i + 5] = payload[1] & (1 << i);
        }
        addressing[13] = payload[2] & 1;

        // L0ID is last 7 bits of payload[2] then first of payload[3]
        char L0ID = (payload[3] << 7) | (payload[2] >> 1);
        this->L0ID = static_cast<unsigned char>(bitReverse(L0ID));
    }
    if (signalType == L1) {
        m_toXORWith = new char[2]();
    } else {
        m_toXORWith = new char[4]();
    }
}

HCC::signalPacket::~signalPacket() {
    delete[] m_bitstring;
    delete[] m_toXORWith;
}

const char* HCC::signalPacket::getBitstring() {
    // generate bitstring from stored data
    if (m_bitstring != nullptr)
        delete[] m_bitstring;
    if (signalType == L1) {
        m_bitstring = new char[2]();
        m_bitstring[0] = bitReverse(0xC0 | (L0ID >> 3));
        m_bitstring[1] = bitReverse(L0ID << 5);
    } else {
        m_bitstring = new char[4]();
        m_bitstring[0] = 0x05; // first 3 bits
        for (int i = 3; i < 8; i++) {
            if (addressing[i - 3])
                m_bitstring[0] |= (1 << i);
        }
        for (int i = 0; i < 8; i++) {
            if (addressing[i + 5]) {
                m_bitstring[1] |= (1 << i);
            }
        }
        if (addressing[13])
            m_bitstring[2] |= (1 << 7);
    }
    return m_bitstring;
}

void HCC::signalPacket::xorWith(const char* complement) {
    for (int i = 0; i < length(); i++) {
        m_toXORWith[i] ^= complement[i];
    }
}

int HCC::signalPacket::bitLength() {
    if (signalType == L1)
        return 12;
    else
        return 26;
}

HCC::HCC(const configuration& conf) : config(conf) {
    std::cout << "Constructing HCC" << std::endl;
    std::cout << "There are " << config.abc130Configurations.size() << " ABC130s" << std::endl;
    // construct each ABC130, and store pointer
    for (auto&& abc130Conf : config.abc130Configurations) {
        abc130s.push_back(new ABC130(abc130Conf));
    }
}

HCC::~HCC() {
    for (auto&& abc130 : abc130s) {
        delete abc130;
    }
}

void HCC::process(Chip::packet* signal, std::vector<Chip::packet*>& replyPackets) {
    // delegate
    process(static_cast<signalPacket*>(signal), replyPackets);
}

void HCC::process(signalPacket* signal, std::vector<Chip::packet*>& replyPackets) {
    if (signal->signalType == signalPacket::L1) {
        std::mt19937 randDev(65489); // Can change the seed
        std::bernoulli_distribution coin;
        // convert signalPacket to ABC130::signalPacket, and send to each ABC130
        for (auto&& abc130 : abc130s) {
            std::vector<Chip::packet*> replies;
            ABC130::signalPacket ABCSignal(signal->getBitstring(), signal->length());
            abc130->process(&ABCSignal, replies);
            // Add replies to replyPackets
            if (config.interleave) {
                std::merge(replyPackets.begin(), replyPackets.end(), replies.begin(), replies.end(),
                           std::back_inserter(replyPackets),
                           [&coin, &randDev](Chip::packet*, Chip::packet*) {
                               return coin(randDev);
                           });
            } else {                               
                replyPackets.insert(replyPackets.end(), replies.begin(), replies.end());
            }
        }
    } else {
        if (!signal->addressing[addressToPos(config.address)]) {
            std::cout << "NOT ADDRESSED TO ME" << std::endl;
            return;
        }
        // Need to remove bits 3 to 16
        std::bitset<26> R3signal = to_bitset<26>(signal->getBitstring());
        std::bitset<12> R3sSignal;
        for (int i = 0; i < 3; i++) {
            R3sSignal[i] = R3signal[i];
        }
        for (int i = 3; i < 12; i++) {
            R3sSignal[i] = R3signal[i + 14];
        }
        char* R3sptr = to_charptr<12>(R3sSignal);
        // For interleave mode
        std::mt19937 randDev(65489); // Can change the seed
        std::bernoulli_distribution coin;
        for (auto&& abc130 : abc130s) {
            // convert to ABC130::signalPacket, and send to ABC130s
            std::vector<Chip::packet*> replies;
            ABC130::signalPacket ABCSignal(R3sptr, 2);
            abc130->process(&ABCSignal, replies);
            std::cout << "Received " << replies.size() << " reply packets" << std::endl;
            // Add replies to replyPackets
            if (config.interleave) {
                std::merge(replyPackets.begin(), replyPackets.end(), replies.begin(), replies.end(),
                           std::back_inserter(replyPackets),
                           [&coin, &randDev](Chip::packet*, Chip::packet*) {
                               return coin(randDev);
                           });
            } else {                               
                replyPackets.insert(replyPackets.end(), replies.begin(), replies.end());
            }
        }

        delete[] R3sptr;
    }
}
