// Header for HCC Chip
// Author: Beojan Stanislaus <beojan.stanislaus@queens.ox.ac.uk>
// This header contains the implementation of the HCC chip
#ifndef HCC_H
#define HCC_H

#include <vector>

#include "../Chip.h"
#include "ABC130.h"
#include <msgpack.hpp>

/** \brief HCC Chip.
 *
 *  This class implements the HCC chip.
 */
class HCC : public Chip {
  public:
    /// \brief HCC Configuration
    struct configuration {
        /// \brief Interleave ABC130 replies
        bool interleave = false;
        /// \brief HCC address
        unsigned char address;
        /// \brief Vector of ABC130 configurations
        std::vector<ABC130::configuration> abc130Configurations;

        /// \brief Get number of ABC130s have configurations
        inline const int numABC130() { return abc130Configurations.size(); }

        MSGPACK_DEFINE(address, abc130Configurations);
    };

    /// \brief HCC Signal Packet
    class signalPacket : public Chip::packet {
      public:
        enum SignalType { L1, R3 };
        /// \brief Signal type
        SignalType signalType;
        /// \brief L0ID requested
        unsigned char L0ID;
        /// \brief Addressing bits
        std::bitset<14> addressing;
        explicit signalPacket(const char* payload, int payloadLength);
        ~signalPacket();

        // No copy constructor / assignment
        signalPacket(const signalPacket&) = delete;
        signalPacket& operator=(const signalPacket&) = delete;

        const char* getBitstring();
        void xorWith(const char* complement);
        int bitLength();

      private:
        char* m_toXORWith;
        char* m_bitstring;
    };

    // HCC only passes on ABC130 replies
    // It does not construct any of its own
    // Hence, it does not have a replyPacket member

    explicit HCC(const configuration& conf);
    ~HCC();

    HCC(const HCC&) = delete;
    HCC& operator=(const HCC&) = delete;
    // processing function called from main.cpp
    void process(Chip::packet* signal, std::vector<Chip::packet*>& replyPackets);

    // work delegated to specific processing function
    void process(signalPacket* signal, std::vector<Chip::packet*>& replyPackets);

  private:
    // list of abc130s that this HCC is connected to
    std::vector<ABC130*> abc130s;
    configuration config;
};

#endif
