// Author: Beojan Stanislaus <beojan.stanislaus@queens.ox.ac.uk>
// This file contains functions for constructing chips and signal packets
// The code that depends on the chip being constructed is hence confined to this
// file, and main.cpp contains only generic code.

/** \brief This file contains the if statements for constructing specific chips.
 *
 *  Please add new chip classes to this file.
 */

#include "Chip.h"
#include "utilities.h"

#include <stdexcept>
#include <string>
#include <cassert>
#include <msgpack.hpp>

//#include header file for each chip class here
#include "Chips/ABC130.h"
#include "Chips/HCC.h"

// add case for each chip class to the two functions below
// see ABC130 case for example
Chip* constructChip(std::string chipName, msgpack::object* configMap) {
    // Use msgpack to extract configuration object from configMap, and
    // construct appropriate type of chip, returning a pointer to the
    // newly constructed chip
    if (chipName == "ABC130") {
        ABC130::configuration config;
        try {
            configMap->convert(config);
        } catch (...) {
            throw new std::runtime_error("INVALID CONFIGURATION");
        }
        return new ABC130(config);
    } else if (chipName == "HCC") {
        HCC::configuration config;
        try {
            configMap->convert(config);
        } catch (...) {
            throw new std::runtime_error("INVALID CONFIGURATION");
        }
        return new HCC(config);
    } else {
        throw new std::runtime_error("INVALID CHIP " + chipName);
    }
};

Chip::packet* constructSignal(std::string chipName, const char* payload, int payloadLength) {
    // Construct appropriate type of signal packet, wrapping payload bitstring
    // Payload would have been extracted from a msgpack message from the client
    // in main.cpp
    if (chipName == "ABC130") {
        ABC130::signalPacket* packet = new ABC130::signalPacket(payload, payloadLength);
        return packet;
    } else if (chipName == "HCC") {
        HCC::signalPacket* packet = new HCC::signalPacket(payload, payloadLength);
        return packet;
    } else {
        throw new std::runtime_error("INVALID CHIP NAME (SIGNAL)"); // should not reach here
    }
}
