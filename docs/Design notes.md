Preliminary Notes
==================

Framework
----------
*   Task\_Start / Task\_Rundown
*   Sets up TCP/IP socket for
    *   Signals / data for chip
        *   Pack signals and replies in some format, e.g. [MessagePack](http://msgpack.org/)
    *   Configuration
    *   See later section
*   Instantiates chip class
    *   Chip class inherits from `Chip` abstract base class.
    *   Configuration would be passed to the constructor.
    *   Chip constructor should instantiate classes lower in hierarchy for subemulation.
        `process` method would then itself call the `process` method on these lower chips.
  
*   Repeats: 
    1.  Unpacks data received on signal port, and calls `process` method of chip
    2.  `process` method fills `std::vector<Chip::packet*>&`
        *   `CHIPNAME::replyPacket` and `CHIPNAME::signalPacket` store data in a private array of
            `unsigned char`, and provide public functions to access components
            * use `std::bitset<3>` for hit patterns
            * provide a `xorWith(const unsigned char[])` function to flip bits
            * provide a constructor from `unsigned char[], int len` (signalPacket only)
            * inherit from `Chip::packet`
    3.  Packs replyPackets and returns to DAQ
    4.  Deletes signal and reply packets to avoid memory leak

*   Provide random number facilities
    *   `bool coinFlip(double probability)`: simple (biased) coin flip
    *   `int poissonCoinFlip(double mean, int max)`: number of success from Poisson distribution
                                                     (at least 1, less than or equal max)
    *   `char* randBitPattern(int nOnes,int length)`: return random bit pattern
*   Provide error mode bitflipping function, flipping 1 in flipRate bits on average
    *   `void flipBits(Chip::packet* packet, int flipRate)`

Chip class
------------

    class Chip {
    public:
        class Chip::packet {
        public:
            virtual ~packet();
            virtual void xorWith(const unsigned char[]);
            virtual const unsigned char* getBitstring();
            virtual const int bitLength();
            
            // provides length in bytes
            // in header to allow inlining.
            const int length() {
                if(this->bitLength() % sizeof(char) == 0)
                    return this->bitLength() / sizeof(char);
                else
                    return (this->bitLength() / sizeof(char)) + 1;
            }
        };
        
        virtual ~Chip();
        
        virtual void process(Chip::packet* signal, std::vector<Chip::packet*>&);
    };


Each chip type would have a `CHIPNAME::configuration` struct that its constructor accepts by
reference, containing all configuration information. This object should be copied if the chip
stores its configuration in the same format as it will be lost upon returning from the constructor.

TCP/IP Setup Design
-------------------
It appears RCEs running RTEMS use BSD sockets API. We only act as a server hence do not need to
lookup ATCA addresses.

1. Listen on port 10251 for signals (just an example, can be changed)
2. On accept: fork new process
3. In new process: use MessagePack to unpack data
4. Get header and payload  
    *   *Can use MessagePack `fixstr` type for header field*
    *   *Payload will generally be a bitstring packed as `bin 8` for signals. (works until 
          packet is longer than 255 × 8 = 2040 bits)*  
         *   *For 'CONFIGURE', use `CHIPNAME` as `fixstr`, followed by
              `CHIPNAME::configuration` packed as an array*
             *   *This means `CHIPNAME::configuration` should be serializable. This can be
                  attained by including a `MSGPACK_DEFINE(//members)` line, as explained at
                  <https://github.com/msgpack/msgpack-c/wiki/v1_1_cpp_adaptor#intrusive-approach>*  
                  *The sender must therefore send the array in the same order that members are
                   listed in the `MSGPACK_DEFINE` macro*
                  
         *   *For 'ERRORMODE', simply have a `uint 16` representing rate of flips (can use
             `float 32` if we really need fractional bit flip proportions). 0 for no errors.*
    *   If header is 'SIGNAL', treat payload as signalPacket  
         *If chip not yet configured, send ERROR packet*
         1. Call `CHIPNAME.process(signalPacket)`
         2. If errors mode is on, flip random bits in reply (1 in flipRate bits) 
         3. Pack reply and send over socket  
             *Reply consists of one `bin 8`*
             *Or one `fixstr` or `str 8` for reporting errors*
    *   If header is 'CONFIGURE', reconfigure emulated chip
         1. Delete current instance of chip (if it exists)
         2. Create new instance with new configuration (from payload)
    *   If header is 'ERRORMODE', payload is a numerical field (flipRate)
    *   If header is 'EXIT', clean up
        1. Clean up by deleting dynamically allocated objects
        2. Close socket
        3. return 0
        
Received Packet Format
----------------------
In MessagePack format (all numbers in diagrams are in hex):  
`fixarray` of 2 or 3 elements:

 `92 | A6 | SIGNAL | DC | /payload here/`
 
 `92 | A9 | ERRORMODE | CD | /number here/`
 
 `93 | A9 | CONFIGURE | AX | /chip name here/ |  9X | /config here/`
 
 `A4 | EXIT`

