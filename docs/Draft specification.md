# Capabilities
*    Emulate each level of readout:
     *   ABC130
     *   HCC (connected to 10 ABC130s)
     *   End of stave controller (connected to 13 or 26 HCCs)
     *   Pair of HCCs (individually addressable)
     *   Groups of each of the above chips (individually addressable)
*   Able to swap out these stages for newer iterations of the chips
*   Extensible to add layers in the hierarchy
*   Easily modifiable to accommodate pixel rather than strip assemblies
*   Operation of emulated chip configurable:
    *   Text based configuration file, using, where possible, same terminology
        as chip specifications
    *   Standard configuration of physical chip
    *   Configuration for lower levels in hierarchy
        E.g. if emulating a HCC, configuration for each ABC130
    *   For ABC130: probability of first hit (per channel) P_first,
        and average number of hits nHits (Poisson distributed)
*   Errors mode to introduce random errors into output

# Framework
*   Framework should provide shared features required by all emulated chips, e.g.
    *   Random number generation
    *   Configuration reading / writing
    *   Packing and unpacking communications
    *   Subemulation
         > Higher level chip's emulators will determine output by emulating the lower level chips
         > they are connected to.
        
*   Framework should also handle starting / stopping routines needed by the platform
*   Should be easy to add more chips to be emulated.

# Platform
The emulator will run on one or two reconfigurable cluster elements on the COB. The emulator will,
in the first instance, be implemented in software using the ARM core.

# Communications
The emulator will expect to receive triggers from and transmit readout packets to a DAQ under test
running on another RCE.
> Should be easily modifiable to accommodate DAQ running on external hardware

The emulator will also need to receive configuration / administrative data (e.g. turning errors
mode on and off), and send debugging information (e.g. state of internal parameters, incorrect
input received) on another channel (perhaps to a controlling PC).

# Development
* The emulator will have a modular design, and the intended operation of each module will be
  documented before coding.
> Could also document headers using Doxygen comments

*  Source code and text documents will be held in a version control system. If this is a
   centralized VCS (e.g. SVN), master repository will be held at CERN. If decentralized (Git),
   a copy of the repository will be held at CERN and all commits will be pushed to it.

