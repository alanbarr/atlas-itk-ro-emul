// Main implementation
// Author: Beojan Stanislaus <beojan.stanislaus@queens.ox.ac.uk>
// Execution starts at Task_Start

#include "debug/print.h"
#include "task/Task.h"
#define PRINT dbg_printv

#include <stdio.h>
#include <cstring> //for std::memset
#include <string>
#include <vector>
#include <iostream> //use stdio only for reading/writing sockets
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <unistd.h>
#include <sys/select.h>
#include <exception>
#include <netdb.h> //getprotoent

#include "Chip.h"
#include "utilities.h"
#include <msgpack.hpp> // MessagePack header
#include <msgpack/fbuffer.hpp>
// Arbitrary choice
// Change if you want
const unsigned int PORT = 10251;
Chip* constructChip(std::string chipName, msgpack::object* configMap);
Chip::packet* constructSignal(std::string chipName, const char* payload, int payloadLength);

void sendMessage(std::string message, FILE* file) {
    // Packs message in MessagePack message, and sends it over the socket
    msgpack::fbuffer buffer(file);
    msgpack::pack(buffer, message + "\n");
    fflush(file);
}

void Task_Start(int argc, const char* argv[]) {
    // Set up TCP/IP server
    PRINT("STARTING\n");
    std::cout << "STARTING" << std::endl;
    struct sockaddr_in listenAddress;
    int sock;
    sock = socket(PF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        std::cerr << "ERROR CREATING SOCKET" << std::endl;
        return;
    }
    std::cout << "CREATED SOCKET" << std::endl;
    // clear, then populate, listenAddress
    std::memset(&listenAddress, 0, sizeof(listenAddress));
    listenAddress.sin_family = AF_INET;
    listenAddress.sin_port = htons(PORT);
    listenAddress.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(sock, (struct sockaddr*)(&listenAddress), sizeof(listenAddress)) < 0) {
        std::cerr << "ERROR BINDING SOCKET TO PORT " << PORT << std::endl;
        return;
    }
    std::cout << "BOUND TO PORT " << PORT << std::endl;

    listen(sock, 1); // Do not accept packets sent while we are processing previous packet
    
    // create and clear acceptAddress
    struct sockaddr acceptAddress;
    std::memset(&acceptAddress, 0, sizeof(acceptAddress));
    // will later contain length of acceptAddress
    unsigned long int acceptAddressLen = 0;
    int fd; // File descriptor for new socket
    FILE* socketFile;
    const int tcpLevel = getprotobyname("tcp")->p_proto;
    std::cout << "ENTERING ACCEPT LOOP" << std::endl;
    while (true) {
        // accept connection
        fd = accept(sock, &acceptAddress, &acceptAddressLen);
        if (fd < 0) {
            std::cerr << "ERROR ACCEPTING CONNECTION" << std::endl;
            return;
        }
        // set keepalive option
        unsigned int optval = 1;
        setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &optval, sizeof(optval));
        std::cout << "ACCEPTED CONNECTION" << std::endl;

        socketFile = fdopen(fd, "ab+"); // Open for read and append, binary mode
                                        // Only used for sending data.
        if (!socketFile) {
            std::cerr << "ERROR OPENING SOCKET FILE DESCRIPTOR" << std::endl;
            return;
        }
        std::cout << "OPENED SOCKET" << std::endl;

        // Message read and process loop
        bool configured = false;
        bool quitting = false; // flag when EXIT message is received
        unsigned int flipRate = 0;
        Chip* emulatedChip = nullptr;
        std::string chipName;
        while (!quitting) {
            // Read and unpack signal
            msgpack::unpacker deserializer;
            msgpack::unpacked unpacked;
            // setup set of file descriptors to watch (only contains file
            // descriptor for our socket)
            fd_set readfds;
            FD_ZERO(&readfds);
            FD_SET(fd, &readfds);
            int bytesRead = 0;
            deserializer.reserve_buffer(1024); // read up to 1024 byte at a time
            // Wait until there is data to read on the socket
            int ready = select(fd + 1, &readfds, nullptr, nullptr, nullptr);
            if (ready == -1) {
                std::cout << "ERROR OCCURED IN select() CALL" << std::endl;
                break;
            } else if (ready == 0) {
                std::cout << "TIMED OUT" << std::endl;
                break;
            }
            // read data
            bytesRead = read(fd, deserializer.buffer(), 1024);
            if (bytesRead == 0)
                break; // leave loop if no bytes read
            deserializer.buffer_consumed(bytesRead);
            while (deserializer.next(unpacked)) { // complete MessagePack message received
                std::cout << "RECEIVED MESSAGE" << std::endl;
                // unpack message
                msgpack::object signalMessage(unpacked.get());
                if (signalMessage.type == msgpack::type::STR) {
                    if (signalMessage.as<std::string>() == "EXIT") {
                        std::cout << "EXIT MESSAGE" << std::endl;
                        quitting = true; // set flag
                        if (configured)
                            delete emulatedChip;
                        break; // break from innermost while
                    } else {
                        // Send error packet
                        sendMessage("ERROR: INVALID PACKET", socketFile);
                    }
                } else if (signalMessage.type == msgpack::type::ARRAY) {
                    // Now start accessing signalMessage internals because we don't know
                    // what it contains until we read the header.
                    // validate message size
                    int arrayLen = signalMessage.via.array.size;
                    if ((arrayLen < 2) || (arrayLen > 3)) {
                        sendMessage("ERROR: INVALID PACKET", socketFile);
                        continue; // go to next message
                    }
                    // read header
                    std::string header;
                    try { // convert throws exception if header is not a string
                        signalMessage.via.array.ptr[0].convert(header);
                    } catch (...) {
                        sendMessage("ERROR: INVALID PACKET", socketFile);
                        continue; // go to next message
                    }

                    // switch based on packet type
                    if (header == "CONFIGURE") {
                        if (arrayLen != 3)
                            sendMessage("ERROR: CONFIGURATION MESSAGE TOO SHORT", socketFile);
                        else {
                            try { // convert throws exception if chipName is not a string
                                signalMessage.via.array.ptr[1].convert(chipName);
                            } catch (...) {
                                sendMessage("ERROR: INVALID CONFIG MESSAGE", socketFile);
                                continue; // go to next message
                            }
                            std::cout << "CONFIGURE MESSAGE" << std::endl;
                            // reinitialize random numbers if in repeatable mode
                            #if initToZero
                            utilities::reinitialize();
                            #endif
                            // pull out configuration
                            msgpack::object* payload;
                            payload = &(signalMessage.via.array.ptr[2]);
                            if (payload->type == msgpack::type::ARRAY) {
                                try { // throw exception from constructChip if invalid
                                    // configuration
                                    if (!configured) {
                                        // configure chip
                                        emulatedChip = constructChip(chipName, payload);
                                        configured = true;
                                        sendMessage("CONFIGURED", socketFile);
                                    } else {
                                        // throw out existing chip and configure a new one
                                        delete emulatedChip;
                                        emulatedChip = constructChip(chipName, payload);
                                        sendMessage("CONFIGURED", socketFile);
                                    }
                                } catch (...) {
                                    sendMessage("ERROR: INVALID CONFIG MESSAGE", socketFile);
                                    configured = false;
                                    continue;
                                }
                            } else {
                                sendMessage("ERROR: INVALID CONFIG MESSAGE", socketFile);
                                continue;
                            }
                        }
                    } else if (header == "SIGNAL") {
                        if (!configured) {
                            sendMessage("ERROR: NOT YET CONFIGURED", socketFile);
                            continue;
                        }
                        if (arrayLen != 2) {
                            sendMessage("ERROR: NO SIGNAL PAYLOAD", socketFile);
                            continue;
                        }
                        if (signalMessage.via.array.ptr[1].type != msgpack::type::BIN) {
                            sendMessage("ERROR: INVALID SIGNAL PAYLOAD", socketFile);
                            continue;
                        }
                        std::cout << "SIGNAL MESSAGE" << std::endl;
                        Chip::packet* signalPacket = nullptr;
                        // container for replies
                        std::vector<Chip::packet*> replyPackets;
                        // extract signal payload
                        std::string payload; // msgpack unpacks bin objects into a string
                                             // to avoid issues with allocating memory
                        signalMessage.via.array.ptr[1].convert(payload);
                        try {
                            // generate signal packet, and send to chip for processing
                            signalPacket =
                                constructSignal(chipName, payload.c_str(), payload.size());

                            emulatedChip->process(signalPacket, replyPackets);
                        } catch (const std::exception& e) {
                            sendMessage("ERROR: " + std::string(e.what()), socketFile);
                            delete signalPacket;
                            continue;
                        }
                        // send out all reply packets generated at once
                        msgpack::fbuffer buffer(socketFile);
                        msgpack::packer<msgpack::fbuffer> packer(buffer);
                        for (auto replyPacket : replyPackets) {
                            if (flipRate)
                                utilities::flipBits(replyPacket, flipRate);
                            // Pack and send replyPacket
                            packer.pack_bin(replyPacket->length());
                            packer.pack_bin_body(replyPacket->getBitstring(),
                                                 replyPacket->length());
                            // clean up
                        }
                        fflush(socketFile);
                        // delete reply packets to avoid a memory leak
                        for (auto&& replyPacket : replyPackets) {
                            delete replyPacket;
                        }
                        replyPackets.clear();
                        // delete signal packet
                        delete signalPacket;
                    } else if (header == "ERRORMODE") {
                        if (arrayLen != 2) {
                            sendMessage("ERROR: NO FLIP RATE", socketFile);
                            continue;
                        }
                        if (signalMessage.via.array.ptr[1].type !=
                            msgpack::type::POSITIVE_INTEGER) {
                            sendMessage("ERROR: NO OR NEGATIVE FLIP RATE", socketFile);
                            continue;
                        }
                        std::cout << "ERRORMODE MESSAGE" << std::endl;

                        flipRate = signalMessage.via.array.ptr[1].as<unsigned int>();
                        // set or clear bit flip rate
                        if (flipRate)
                            sendMessage("ERRORMODE SET", socketFile);
                        else
                            sendMessage("ERRORMODE CLEARED", socketFile);
                    } else {
                        sendMessage("ERROR: INVALID HEADER", socketFile);
                        continue;
                    }
                } else {
                    sendMessage("ERROR: INVALID PACKET", socketFile);
                }
            }
        }
        fclose(socketFile); // close socket
        std::cout << "CLOSED SOCKET" << std::endl;
    }
}

void Task_Rundown() {
    // Task end function that will usually never run
    std::cout << "Closing emulator server" << std::endl;
    return;
}
