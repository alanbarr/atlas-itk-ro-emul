#ifndef ABC130_CONFIG_H
#define ABC130_CONFIG_H
#include <msgpack.hpp>

struct ABC130_config {
    unsigned char ChipID;
    int recipHitProb;
    double meanHitsInCluster;
    int L0CounterInitVal;
    int BCCounterInitVal;
    bool L1_1BC;
    bool Edge;
    MSGPACK_DEFINE(ChipID, recipHitProb, meanHitsInCluster, L0CounterInitVal, BCCounterInitVal,
                   L1_1BC, Edge);
};
#endif
