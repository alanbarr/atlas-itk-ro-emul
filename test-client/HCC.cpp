#include <asio.hpp>
#include <msgpack.hpp>

#include <iostream>
#include <string>
#include <bitset>
#include <exception>
#include <thread>
#include <mutex>
#include <functional>
#include <random>
#include <chrono>
#include <cstdint>
#include <cstdlib>
#include <tuple>
#include <limits>
#include <utility>
#include <future>

#include "ABC130_config.h"
#include "HCC_config.h"

void reciever(asio::ip::tcp::socket& socket, std::mutex& mutex) {
    msgpack::unpacker unpacker;
    asio::error_code error;
    int packetNum = 0;
    int maxTime = 0;
    int totalBytes = 0;
    while (true) {
        auto now = std::chrono::high_resolution_clock::now();
        std::size_t bytesAvailable = 0;
        bytesAvailable = socket.available();
        if (bytesAvailable == 0) {
            mutex.lock();
            std::cout << "Received total of " << totalBytes << " bytes" << std::endl;
            std::cout << "Waiting for more..." << std::flush;
            mutex.unlock();
            totalBytes = 0;
        }
        while (bytesAvailable == 0) {
            std::this_thread::sleep_for(std::chrono::microseconds(1500));
            bytesAvailable = socket.available();
        }

        mutex.lock();
        std::cerr << "Waiting to receive " << bytesAvailable << " bytes\n";
        mutex.unlock();
        unpacker.reserve_buffer(bytesAvailable);
        std::size_t bytesRead =
            asio::read(socket, asio::buffer(unpacker.buffer(), bytesAvailable), error);
        if (error) {
            std::cerr << "RECEIVE ERROR: " << error.message() << std::endl;
            return;
        }
        mutex.lock();
        std::cerr << "Received " << bytesRead << " bytes\n";
        mutex.unlock();
        totalBytes += bytesRead;
        unpacker.buffer_consumed(bytesRead);
        mutex.lock();
        msgpack::unpacked unpacked;
        msgpack::object reply;
        while (unpacker.next(unpacked)) {
            reply = unpacked.get();
            if (reply.type == msgpack::type::STR) {
                std::cout << "Received string packet (of " << bytesRead << " byte package)"
                          << std::endl;
                std::cout << reply.as<std::string>();
                std::cout << std::endl
                          << std::endl;
            } else if (reply.type == msgpack::type::BIN) {
                std::cout << "Received binary packet (of " << bytesRead << " byte package)"
                          << std::endl;
                std::cout << "Length: " << reply.via.bin.size << " bytes" << std::endl;
                for (int i = 0; i < reply.via.bin.size; i++) {
                    char byte = reply.via.bin.ptr[i];
                    for (int j = 0; j < 8; j++) {
                        if (byte & (1 << j))
                            std::cout << "1";
                        else
                            std::cout << "0";
                    }
                }
                std::cout << std::endl;
            }
            std::cout << "Packet number: " << ++packetNum << "\n" << std::endl;
        }
        int timeTaken = std::chrono::duration_cast<std::chrono::milliseconds>(
                            std::chrono::high_resolution_clock::now() - now).count();
        maxTime = (maxTime > timeTaken && maxTime < 1000) ? maxTime : timeTaken;
        std::cout << "Max time: " << maxTime << "ms" << std::endl;
        mutex.unlock();
    }
}

int main(int argc, char const* argv[]) {
    using asio::ip::tcp;
    using ms = std::chrono::milliseconds;
    using std::chrono::duration_cast;
    using hrc = std::chrono::high_resolution_clock;
    if (argc != 2) {
        std::cerr << "Must provide ip address of emulator as argument" << std::endl;
        return 1;
    }
    // Setup ASIO
    try {
        asio::io_service io;
        tcp::resolver resolver(io);
        tcp::resolver::query query(argv[1], "10251");
        auto endpointIt = resolver.resolve(query);
        tcp::socket socket(io);
        asio::connect(socket, endpointIt);

        // setup receive thread
        std::mutex mutex;
        std::thread receiveThread(reciever, std::ref(socket), std::ref(mutex));
        mutex.lock();
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<> distHCC(2, 2 * 14 + 1);
        std::cout << "ATLAS Tracker Readout Emulator Test Client" << std::endl;
        std::cout << std::endl;

        std::cout << "Sending HCC Configure message" << std::endl;
        msgpack::sbuffer msgpackBuffer;
        msgpack::packer<msgpack::sbuffer> packer(msgpackBuffer);

        HCC_config config;
        config.address = distHCC(rd);
        std::cout << "Address: " << std::bitset<5>(config.address) << std::endl;
        auto&& abc130s = config.abc130Configurations;
        for (int i = 0; i < 10; i++) {
            abc130s.emplace_back();
            auto&& abc130 = abc130s.back();
            abc130.ChipID = i;
            abc130.recipHitProb = 2000;
            abc130.meanHitsInCluster = 1.4;
            abc130.L0CounterInitVal = 0;
            abc130.BCCounterInitVal = 0;
            abc130.L1_1BC = true;
            ;
            abc130.Edge = 0;
        }

        auto configMsg = std::make_tuple("CONFIGURE", "HCC", std::move(config));
        packer.pack(configMsg);
        std::cout << "Local config msg size is " << sizeof(configMsg) << std::endl;
        std::cout << "Configure message size is " << msgpackBuffer.size() << std::endl;
        mutex.unlock();
        asio::write(socket, asio::buffer(msgpackBuffer.data(), msgpackBuffer.size()));
        msgpackBuffer.clear();
        std::this_thread::sleep_for(ms(100));
        mutex.lock();
        std::cout << "Sending 1000 R3 requests" << std::endl;
        ms totalTime(0);
        std::uniform_int_distribution<> distL0(0, 255);
        for (int idx = 0; idx < 1000; ++idx) {
            msgpackBuffer.clear();
            packer.pack_array(2);
            packer.pack("SIGNAL");
            std::bitset<26> signal;
            signal[0] = 1;
            signal[1] = 0;
            signal[2] = 1;
            // char n = std::get<2>(configMsg).address;
            // signal[3 + ((n % 2) ? (n - 1) / 2 : n / 2)] = 1;
            for (int i = 0; i < 14; i++) {
                signal[i + 3] = 1;
            }
            char L0ID = distL0(gen);
            for (int i = 0; i < 8; i++) {
                signal[i + 17] = L0ID & (1 << (7 - i));
            }
            signal[25] = 0;
            unsigned long long sigLong = signal.to_ullong();
            packer.pack_bin(4);
            // Only works because x86_64 is little endian
            packer.pack_bin_body((char*)(&sigLong), 4);
            std::cout << "\n\nSending: ";
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 8; j++) {
                    int num = 8 * i + j;
                    std::cout << (((char*)(&sigLong))[i] & (1 << j) ? "1" : "0");
                    if (num == 2 || num == 16 || num == 25) {
                        std::cout << " ";
                    }
                }
            }
            std::cout << "\nPress enter when packets have been received" << std::endl;
            mutex.unlock();
            auto startTime = hrc::now();
            asio::write(socket, asio::buffer(msgpackBuffer.data(), msgpackBuffer.size()));
            std::string temp;
            auto fut = std::async(std::launch::async, [&] {
                std::getline(std::cin, temp);
                return 0;
            });
            fut.wait();
            mutex.lock();
            totalTime += duration_cast<ms>(hrc::now() - startTime);
        }
        msgpackBuffer.clear();
        std::cout << "R3 Response took on average " << totalTime.count() / 1000 << "ms"
                  << std::endl;
        std::cout << "Sleeping for 10s..." << std::endl;
        mutex.unlock();
        std::this_thread::sleep_for(ms(10000));
        mutex.lock();
        std::cout << "Slept for 10s, press ENTER to continue." << std::endl;
        std::string temp2;
        std::cout << ">>>";
        std::cout.flush();
        std::getline(std::cin, temp2);
        std::cout << "Sending 1000 L1 requests" << std::endl;
        for (int idx = 0; idx < 1000; ++idx) {
            msgpackBuffer.clear();
            packer.pack_array(2);
            packer.pack("SIGNAL");
            std::bitset<12> signal;
            signal[0] = 1;
            signal[1] = 1;
            signal[2] = 0;
            char L0ID = distL0(gen);
            for (int i = 0; i < 8; i++) {
                signal[i + 3] = L0ID & (1 << (7 - i));
            }
            signal[11] = 0;
            unsigned long long sigLong = signal.to_ullong();
            packer.pack_bin(2);
            // Only works because x86_64 is little endian
            packer.pack_bin_body((char*)(&sigLong), 2);
            std::cout << "Sending: ";
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 8; j++) {
                    int num = 8 * i + j;
                    std::cout << (((char*)(&sigLong))[i] & (1 << j) ? "1" : "0");
                    if (num == 2 || num == 16 || num == 25) {
                        std::cout << " ";
                    }
                }
            }
            std::cout << "\nPress enter when packets have been received" << std::endl;
            mutex.unlock();
            auto startTime = hrc::now();
            asio::write(socket, asio::buffer(msgpackBuffer.data(), msgpackBuffer.size()));
            std::string temp;
            auto fut = std::async(std::launch::async, [&] {
                std::getline(std::cin, temp);
                return 0;
            });
            fut.wait();
            mutex.lock();
            totalTime += duration_cast<ms>(hrc::now() - startTime);
        }
        msgpackBuffer.clear();
        std::cout << "Response took on average " << totalTime.count() / 1000 << "ms" << std::endl;

    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return 2;
    }

    return 0;
}
