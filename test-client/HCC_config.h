// Header for HCC Chip
#ifndef HCC_CONFIG_H
#define HCC_CONFIG_H

#include <vector>

#include "ABC130_config.h"
#include <msgpack.hpp>

struct HCC_config {
    /// \brief Interleave ABC130 replies
    bool interleave = false;
    
    /// \brief HCC address
    unsigned char address;
    /// \brief Vector of ABC130 configurations
    std::vector<ABC130_config> abc130Configurations;

    /// \brief Get number of ABC130s have configurations
    inline const int numABC130() { return abc130Configurations.size(); }

    MSGPACK_DEFINE(address, abc130Configurations);
};

#endif
