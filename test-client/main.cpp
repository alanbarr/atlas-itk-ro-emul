#include <asio.hpp>
#include <msgpack.hpp>

#include <iostream>
#include <string>
#include <bitset>
#include <exception>
#include <thread>
#include <mutex>
#include <functional>
#include <chrono>
#include <cstdint>
#include <cstdlib>

#include "ABC130_config.h"

void reciever(asio::ip::tcp::socket& socket, std::mutex& mutex) {
    msgpack::unpacker unpacker;
    asio::error_code error;
    int packetNum = 0;
    int maxTime = 0;
    while (true) {
        auto now = std::chrono::high_resolution_clock::now();
        std::size_t bytesAvailable = 0;
        while (bytesAvailable == 0) {
            std::this_thread::sleep_for(std::chrono::milliseconds(5));
            bytesAvailable = socket.available();
        }

        mutex.lock();
        std::cerr << "Waiting to receive " << bytesAvailable << " bytes\n";
        mutex.unlock();
        unpacker.reserve_buffer(bytesAvailable);
        std::size_t bytesRead =
            asio::read(socket, asio::buffer(unpacker.buffer(), bytesAvailable), error);
        if (error) {
            std::cerr << "RECEIVE ERROR: " << error.message() << std::endl;
            return;
        }
        mutex.lock();
        std::cerr << "Received " << bytesRead << " bytes\n";
        mutex.unlock();
        unpacker.buffer_consumed(bytesRead);
        mutex.lock();
        msgpack::unpacked unpacked;
        msgpack::object reply;
        while (unpacker.next(unpacked)) {
            reply = unpacked.get();
            if (reply.type == msgpack::type::STR) {
                std::cout << "Received string packet (of " << bytesRead << " byte package)"
                          << std::endl;
                std::cout << reply.as<std::string>();
                std::cout << std::endl
                          << std::endl;
            } else if (reply.type == msgpack::type::BIN) {
                std::cout << "Received binary packet (of " << bytesRead << " byte package)"
                          << std::endl;
                std::cout << "Length: " << reply.via.bin.size << " bytes" << std::endl;
                for (int i = 0; i < reply.via.bin.size; i++) {
                    char byte = reply.via.bin.ptr[i];
                    for (int j = 0; j < 8; j++) {
                        if (byte & (1 << j))
                            std::cout << "1";
                        else
                            std::cout << "0";
                    }
                }
                std::cout << std::endl;
            }
            std::cout << "Packet number: " << ++packetNum << "\n" << std::endl;
        }
        int timeTaken = std::chrono::duration_cast<std::chrono::milliseconds>(
                            std::chrono::high_resolution_clock::now() - now).count();
        maxTime = (maxTime > timeTaken && maxTime < 1000) ? maxTime : timeTaken;
        std::cout << "Max time: " << maxTime << "ms" << std::endl;
        mutex.unlock();
    }
}

int main(int argc, char const* argv[]) {
    using asio::ip::tcp;
    if (argc != 2) {
        std::cerr << "Must provide ip address of emulator as argument" << std::endl;
        return 1;
    }
    // Setup ASIO
    try {
        asio::io_service io;
        tcp::resolver resolver(io);
        tcp::resolver::query query(argv[1], "10251");
        auto endpointIt = resolver.resolve(query);
        tcp::socket socket(io);
        asio::connect(socket, endpointIt);

        // setup receive thread
        std::mutex mutex;
        std::thread receiveThread(reciever, std::ref(socket), std::ref(mutex));
        mutex.lock();
        std::cout << "ATLAS Tracker Readout Emulator Interactive Test Client" << std::endl;
        std::cout << "NOTE: THIS PROGRAM IS NOT RESILIENT TO BAD INPUT FROM THE USER" << std::endl;
        std::cout << std::endl;
        mutex.unlock();
        while (true) {
            mutex.lock();
            std::cout << "Select message type" << std::endl;
            enum { CONFIGURE = 1, SIGNAL = 2, ERRORMODE = 3, EXIT = 4 };
            int msgType;
            std::cout << "1. CONFIGURE, 2. SIGNAL, 3. ERRORMODE, 4. EXIT, 5. RECEIVE" << std::endl;
            std::cout << "-> ";
            std::cin >> msgType;
            if (msgType == 5) {
                mutex.unlock();
                std::this_thread::sleep_for(std::chrono::seconds(1));
                mutex.lock();
                continue;
            }
            // Error handling
            while (msgType < 1 || msgType > 4) {
                std::cout << "Invalid choice" << std::endl;
                std::cout << "-> ";
                std::cin >> msgType;
            }
            msgpack::sbuffer msgpackBuffer;
            msgpack::packer<msgpack::sbuffer> packer(msgpackBuffer);
            switch (msgType) {
            case CONFIGURE: { // scoping
                packer.pack_array(3);
                msgpack::pack(packer, "CONFIGURE");
                msgpack::pack(packer, "ABC130");
                ABC130_config config;
                std::string input;
                std::cin.ignore(100, '\n'); // remove newline
                std::cout << "Enter 5 bit chip ID: ";
                std::getline(std::cin, input);
                config.ChipID = static_cast<unsigned char>(std::bitset<5>(input).to_ulong());
                std::cout << "Enter reciprocal of hit probability per channel: ";
                std::cin >> config.recipHitProb;
                std::cout << "Enter mean number of hits in cluster (1 - 4): ";
                std::cin >> config.meanHitsInCluster;
                std::cout << "Enter initial value of L0 counter: ";
                std::cin >> config.L0CounterInitVal;
                std::cout << " Enter initial value of BC counter: ";
                std::cin >> config.BCCounterInitVal;
                std::cout << "Enter L1_1BC value (1/0): ";
                std::cin >> config.L1_1BC;
                std::cout << "Edge (0-1 transition) Detection? (1/0): ";
                std::cin >> config.Edge;
                std::cout << std::endl;
                msgpack::pack(packer, config);

                break;
            }
            case SIGNAL: {
                packer.pack_array(2);
                msgpack::pack(packer, "SIGNAL");
                bool L1;
                std::cout << "Enter 1 for L1, 0 for R3: ";
                std::cin >> L1;
                std::cout << "ENTER L0ID (0 - 255): ";
                std::uint8_t L0ID;
                int L0IDinput;
                std::cin >> L0IDinput;
                L0ID = L0IDinput;
                packer.pack_bin(2);
                char payloadRev[2];
                char payload[2];
                // First byte is 101 or 110 then first 5 bits of L0ID
                payloadRev[0] = (L1 ? 0xC0 : 0xA0) | (L0ID >> 3);
                // Second byte is last 3 bits of L0ID, then zeros
                payloadRev[1] = L0ID << 5;
                // Now reverse each byte
                payload[0] = 0;
                payload[1] = 0;
                for (int i = 0; i < 8; i++) {
                    if (payloadRev[0] & (1 << i))
                        payload[0] |= (1 << (7 - i));
                    if (payloadRev[1] & (1 << i))
                        payload[1] |= (1 << (7 - i));
                }

                packer.pack_bin_body(payload, 2);
                break;
            }
            case ERRORMODE: {
                packer.pack_array(2);
                msgpack::pack(packer, "ERRORMODE");
                int flipRate;
                std::cout << "Enter reciprocal of flip rate (i.e. 1 in n): ";
                std::cin >> flipRate;
                msgpack::pack(packer, flipRate);
                break;
            }
            case EXIT: {
                msgpack::pack(packer, "EXIT");
                break;
            }
            }

            mutex.unlock();
            for (int idx = 0; idx < 1000000; ++idx) {
                asio::write(socket, asio::buffer(msgpackBuffer.data(), msgpackBuffer.size()));
                if (msgType != SIGNAL)
                    idx = 1000000;
                std::this_thread::sleep_for(std::chrono::milliseconds(50));
            }
            msgpackBuffer.clear();

            if (msgType == EXIT) {
                std::exit(0); // exit program if we have sent EXIT message
            }
        }
        receiveThread.join();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return 2;
    }

    return 0;
}
