// Misc utilities (random number)
#include "utilities.h"
#include "Chip.h"

#include <cstring>
#include <math.h> //to use rounding functions
#include <ctime>  //use time to seed PRNG
#include <random>
#include <new>

// define static variables
bool utilities::initialized = false;
std::mt19937 utilities::randEngine;

constexpr int toByteLength(int bitLength) {
    return bitLength % 8 == 0 ? bitLength / 8 : bitLength / 8 + 1;
}

void setBit(char* pattern, int n) {
    // sets bit n (starting at 0)
    // assumes pattern has sufficient length
    pattern[n / 8] |= (1 << (n % 8));
}

bool checkBit(char* pattern, int n) {
    // checks bit n (starting at 0)
    // assumes pattern has sufficient length
    return pattern[n / 8] & (1 << (n % 8));
}

void utilities::initialize() {
    if (initialized)
        return;
    if (initToZero) // defined on command line
        randEngine.seed(0);
    else 
        randEngine.seed(std::clock());
    initialized = true;
}

#if initToZero
void utilities::reinitialize() {
    randEngine.seed(0);
}
#endif

bool utilities::coinFlip(double probability) {
    if (!initialized)
        initialize();
    std::bernoulli_distribution distribution(probability);
    return distribution(randEngine);
}

int utilities::poissonCoinFlip(double mean, int max) {
    if (!initialized)
        initialize();
    std::poisson_distribution<> distribution(mean);
    int ans = distribution(randEngine);
    if (ans < 1)
        ans = 1;
    if (ans > max)
        ans = max;
    return ans;
}

char* utilities::randBitPattern(int nOnes, int length) {
    if (!initialized)
        initialize();
    if (nOnes > length)
        nOnes = length;
    char* pattern = new char[toByteLength(length)];
    std::memset(pattern, 0, toByteLength(length)); // clear pattern
    std::uniform_int_distribution<> distribution(0, length - 1);
    for (int i = 0; i < nOnes; i++) {
        int location = 0;
        do {
            location = distribution(randEngine);
        } while (checkBit(pattern, location));
        setBit(pattern, location);
    }

    return pattern;
}

void utilities::flipBits(Chip::packet* packet, int flipRate) {
    if (flipRate <= 0)
        return;
    int nOnes = static_cast<int>(nearbyint(packet->bitLength() / static_cast<double>(flipRate)));
    char* pattern = randBitPattern(nOnes, packet->bitLength());
    packet->xorWith(pattern);
    delete[] pattern;
}
