// Misc utilities
// Author: Beojan Stanislaus <beojan.stanislaus@queens.ox.ac.uk>
// This header implements the utilities class, containing the utility
// functions that make use of random numbers
//
// It also implements functions that convert an std::bitset to an array
// of char, and vice-versa
#ifndef UTILITIES_H
#define UTILITIES_H
#include "Chip.h"

#include <random>
#include <bitset>

/** \brief Convert bitset to char*.
 *
 *  \param bits Bitset
 */
// templated function, so implementation is in header
template <int n> char* to_charptr(const std::bitset<n>& bits) {
    constexpr int byteLength = n % 8 == 0 ? n / 8 : n / 8 + 1;
    char* pattern = new char[byteLength]();
    // copy bit by bit
    for (int i = 0; i < n; i++) {
        if (bits.test(i))
            pattern[i / 8] |= (1 << (i % 8)); // set bit i;
    }
    return pattern;
}

/** \brief Convert char* to bitset
 *
 *  It is the user's responsibility to ensure the bitstring is of the
 *  appropriate length
 *
 *  \param str Bitstring
 */
template <int n> std::bitset<n> to_bitset(const char* str) {
    constexpr int byteLength = n % 8 == 0 ? n / 8 : n / 8 + 1;
    std::bitset<n> bits;
    // copy bit by bit
    for (int i = 0; i < byteLength; i++) {
        for (int j = 0; j < 8; j++) {
            if (8 * i + j >= n)
                break;
            bits[8 * i + j] = str[i] & (1 << j);
        }
    }
    return bits;
}

/** Utility class, containing only static members. */
class utilities {
  public:
    /** \brief Flip a coin.
     *
     *  \param probability Success probability.
     */
    static bool coinFlip(double probability);

    /** \brief Draw number of coin flip successes from Poisson distribution.
     *
     *  \param mean Mean number of successes.
     *  \param max Max number of successes.
     */
    static int poissonCoinFlip(double mean, int max);

    /** \brief Flip 1 in flipRate bits of packet.
     *
     *  \param[in,out] packet Packet to flip bits of.
     *  \param[in] flipRate Proportion of bits to flip.
     */
    static void flipBits(Chip::packet* packet, int flipRate);

    /** \brief Generate random bit pattern.
     *
     *  The caller is responsible for deleting the bit pattern after use (using delete[])
     *  \param nOnes Number of ones.
     *  \param length Length of pattern in bits.
     */
    static char* randBitPattern(int nOnes, int length);
    
    // Helper for repeatability 
    #if initToZero
    static void reinitialize();
    #endif

  private:
    // remember if randEngine has been initialized
    static bool initialized;
    // standard Mersenne twister pseudo-random number generator
    static std::mt19937 randEngine;
    // initialize randEngine
    static void initialize();
};
#endif
